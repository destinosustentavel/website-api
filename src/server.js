import http from 'http';
import './bootstraps';
import { app } from './app';

const port = process.env.APP_PORT || 3000;

const server = http.createServer(app);

server.listen(port);

server.on('error', (err) => {
  console.log('[SERVER ERROR]', err);
});
server.on('listening', () => {
  console.log('[Server ON] : ' + 'http://..:' + port);
});
