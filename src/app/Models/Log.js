import mongoose from 'mongoose';
mongoose.set('useCreateIndex', true);
const logSchema = new mongoose.Schema(
  {
    userId: {
      type: String,
    },
    action: {
      type: String,
    },
    from: {
      type: String,
    },
  },

  {
    timestamps: true,
  }
);

export default mongoose.model('Log_Collection', logSchema);
