import mongoose from 'mongoose';
mongoose.set('useCreateIndex', true);
const coords = new mongoose.Schema({
    type: {
        default: "Point",
        type: String
    },
    coordinates: [Number, Number, Number],
    address: String,
    name: String
});
export default coords;