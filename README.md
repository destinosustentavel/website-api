# Destino Sustentável (Sustainable Destination) - API :green_heart:

Start of development of the public Api for the **Destino Sustentável (Sustainable Destination)** project.  
[Visit our page! ](https://www.destinosustentavel.org/)

## 📝 About us

to do

## :wrench: Getting started

```bash
# clone repository
git clone https://gitlab.com/destinosustentavel/website-api.git

# install dependencies
npm install / yarn install

# run aplication
npm run dev / yarn dev
```

## :bookmark_tabs: Status Project

_This project is in Development_
