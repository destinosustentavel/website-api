import { Router } from 'express';
import SessionController from '../app/Controllers/SessionController';
import SessionMiddleware from '../app/middlewares/SessionMiddleware';

const sessionRoutes = Router();

sessionRoutes.post('/login', SessionController.create);
sessionRoutes.post('/sessions/refresh_token', SessionController.update); // UPDATE SESSION

sessionRoutes.use(SessionMiddleware);

sessionRoutes.post('/sessions/logout', SessionController.remove);

export default sessionRoutes;
