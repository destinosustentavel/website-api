import React from 'react';

// import { Container } from './styles';

function MainContent({ imgBgSrc = '', titleText = '', subTitle = '' }) {
  return (
    <section className="main-content">
      <div>
        <div className="title-group">
          <h1>{`${titleText}`}</h1>
          <h3>{`${subTitle}`}</h3>
        </div>
        <img src={imgBgSrc} className="img-fluid" alt="img" />
      </div>
    </section>
  );
}

export default MainContent;
