import mongoose from 'mongoose';
import { mongoConnection } from './mongoConnection';

class database {
  constructor() {
    this.init();
  }
  init() {
    this.Connections();
  }

  Connections() {
    mongoConnection();
  }
}
export default new database();
