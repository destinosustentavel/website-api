import jwt from 'jsonwebtoken';
import fs from 'fs';
import path from 'path';

export const decodeTokenJwt = async (token) => {
  try {
    var pub = fs.readFileSync(
      path.join(__dirname, '..', '..', '..', 'certificates', 'pub.pem')
    );

    const decoded = jwt.verify(token, pub, {
      algorithms: ['RS256'],
      // ignoreExpiration: true
    });
    return decoded;
  } catch (error) {
    console.log(error);
    return false;
  }
};
