# Setting up you Keycloak container

Run tihs script in terminal to setup you keycloak container

```
docker run -d -p 8080:8080 --name kc_ds makusdouglas/kc_ds
```

if you want to setup a container from zero, run this command in you terminal

```
docker run -d -p 8080:8080 -e KEYCLOAK_USER=admin -e KEYCLOAK_PASSWORD=admin quay.io/keycloak/keycloak:11.0.3
```
