import { Router } from 'express';
import UserAdsController from '../app/Controllers/UserAdsController';
import UserController from '../app/Controllers/UserController';
import SessionMiddleware from '../app/middlewares/SessionMiddleware';

const userRoutes = Router();

userRoutes.post('/register', UserController.create); // LIST USERS

userRoutes.use(SessionMiddleware);

userRoutes.get('/', UserController.index); // LIST USERS
userRoutes.patch('/update', UserController.update); // UPDATE USERS
// userRoutes.delete('/delete/:id', UserController.remove); // DELETE USERS
userRoutes.patch('/disable/:id', UserController.disable); // DISABLE USERS
userRoutes.patch('/enable/:id', UserController.enable); // ENAMBLE USERS

//userAds
userRoutes.patch('/adsubscribe/:ad_id', UserAdsController.create);

export default userRoutes;
