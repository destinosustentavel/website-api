import mongoose from 'mongoose';
mongoose.set('useCreateIndex', true);
import notificationSchema from './schemas/notificationSchema';
import coords from './schemas/coords';
const userSchema = new mongoose.Schema(
  {
    kc_id: {
      type: String,
      required: true,
      unique: true,
      index: true,
    },
    active: {
      type: Boolean,
      default: true,
    },
    username: {
      type: String,
      required: true
    },
    name: {
      type: String,
      required: true,
    },
    email: {
      type: String,
      required: true,
      unique: true,
      index: true,
    },
    pontuation: {
      type: Number,
      default: 0.0,
    },
    PJ_associated: {
      type: String,
    },
    interest: {
      type: String,
    },
    cep: {
      type: String,
      default: null,
    },
    online: {
      type: Boolean,
      default: false,
    },
    phone: {
      type: String,
    },
    photo: {
      type: String,
    },
    type: {
      type: String,
      required: true,
    },
    token: {
      type: Array,
    },
    attended_ads: [String],
    conversations: [String],
    notifications: [notificationSchema],
    origin: {
      type: String,
      default: 'undefined'
    },
    location: [coords] 
    /**
      [
        {
        "type": "Point",
        "coordinates": [1, 2, 3],
        "address": "depot",
        "name": "Depósito padrão"
        }
      ]
     * 
     */
  },

  {
    timestamps: true,
  }
);

export default mongoose.model('User_Collection', userSchema);
