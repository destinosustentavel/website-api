import { Router } from 'express';

//Middlewares
import SessionMiddleware from '../app/middlewares/SessionMiddleware';
// Routes
import adsRoutes from './adsRoutes';
import userRoutes from './userRoutes';
import sessionRoutes from './sessionRoutes';

const routes = Router();

routes.get('/insomnia', (req, res) => {
  return res.render('insomnia')
});
routes.get('/home', (req, res) => {
  return res.render('home', {
    title: {
      br: 'Bem Vindo à Api do Destino Sustentável',
      en: 'Welcome to Sustainable Destination API',
    },
  });
});
routes.get('/', (_, res) => {
  res.redirect('/home');
});

// USER CRUD
// routes.post('/register', UserController.create); // CREATE USERS
routes.use('/users', userRoutes);
routes.use('/', sessionRoutes);

routes.use(SessionMiddleware); // A parti daqui as rotas são protegidas

routes.use('/ads', adsRoutes);

// # TODO:
// - middleware que faz refresh no token de usuario, ao acessar uma rota

// ADS ROUTES
// routes.put

export { routes };
