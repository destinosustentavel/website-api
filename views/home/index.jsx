import React from 'react';
// components
import Hello from '../components/hello';
import Header from '../components/Header';
import MainContent from '../components/MainContent';

function index({ title }) {
  return (
    <html>
      <head>
        <meta charSet="UTF-8" />
        <meta name="viewport" content="width=device-width, initial-scale=1.0" />

        <link rel="shortcut icon" href="assets/fav.svg" type="image/x-icon" />
        {/* Css */}
        <link rel="stylesheet" href="css/bootstrap.min.css" />
        <link
          href="https://fonts.googleapis.com/css2?family=Passion+One:wght@700&display=swap"
          rel="stylesheet"
        ></link>
        <link rel="stylesheet" href="styles/index.css" />
        <link rel="stylesheet" href="styles/group_components.css" />
        <title>Ds API</title>
      </head>
      <body>
        <Header>
          <section>
            <h1>{title.br}</h1>
          </section>
          <hr />
        </Header>
        <MainContent
          titleText="API DESTINO SUSTENTÁVEL"
          subTitle="Reciclar para recriar o futuro."
          imgBgSrc="assets/backgrounds/hero.png"
        />
      </body>

      <script
        src="https://code.jquery.com/jquery-3.5.1.slim.min.js"
        integrity="sha384-DfXdz2htPH0lsSSs5nCTpuj/zy4C+OGpamoFVy38MVBnE+IbbVYUew+OrCXaRkfj"
        crossOrigin="anonymous"
      ></script>
      <script
        src="https://cdn.jsdelivr.net/npm/bootstrap@4.5.3/dist/js/bootstrap.bundle.min.js"
        integrity="sha384-ho+j7jyWK8fNQe+A12Hb8AhRq26LrZ/JpcUGGOn+Y7RsweNrtN/tE3MoK7ZeZDyx"
        crossOrigin="anonymous"
      ></script>
    </html>
  );
}

export default index;
