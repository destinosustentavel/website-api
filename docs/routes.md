# Routes to use the API

## Sessions

### /login (public)

Entrys:

```
{
    username: "my_username",
    password: "my_password"
}
```

Returns:

```
{
  "accessToken": "access token here",
  "refreshToken": "refresh token here",
  "exp": 1606600865,
  "keyclockData": {
    "id": "userId",
    "createdTimestamp": 1603249262573,
    "username": "myUsername",
    "enabled": bool,
    "totp": false,
    "emailVerified": bool,
    "email": "myuser@gmail.com",
    "disableableCredentialTypes": [],
    "requiredActions": [],
    "notBefore": 0,
    "access": {
      "manageGroupMembership": bool,
      "view": bool,
      "mapRoles": bool,
      "impersonate": bool,
      "manage": bool
    }
  }
}
```

### /sessions/refresh_token (public)

### /sessions/logout lock (private)
