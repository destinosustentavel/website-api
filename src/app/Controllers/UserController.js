import User from '../Models/User';
import KeycloakAdmin from '../../lib/kcAdmin';
import keycloakAdmin from '../../lib/kcAdmin';

// import keycloakClient from '../../config/keycloakClient';
// import kcClientToken from '../../config/keycloakClient/clientTokenManager';

export default {
  async create(req, res) {
    const {
      username,
      name,
      email,
      password,
      evaluation,
      interest,
      latitude,
      longitude,
      altitude,
      cep,
      online,
      phone,
      photo,
      type,
      conversations,
      notifications,
      origin = 'undefined',
    } = req.body;

    //Verifica se token é válido

    if (
      !(
        username &&
        name &&
        email &&
        interest &&
        latitude &&
        longitude &&
        phone &&
        type
      )
    ) {
      return res.status(401).json({
        error: 'required fields are empty',
      });
    }
    let userData;

    const hasUser = await User.findOne({
      email,
    });
    console.log(hasUser);

    if (hasUser) {
      return res.status(401).json({
        error: 'email already exists',
      });
    }

    const kcAdmin = await KeycloakAdmin();

    const [alreadyUsedUserName] = await kcAdmin.users.find({
      username,
      realm: process.env.KC_REALM_NAME,
    });

    if (alreadyUsedUserName) {
      return res.json({
        error: `Email or username already in use`,
      });
    } else {
      const [isValalreadyUsedEmail] = await kcAdmin.users.find({
        email,
        realm: process.env.KC_REALM_NAME,
      });
      if (isValalreadyUsedEmail) {
        return res.json({
          error: `Email or username already in use`,
        });
      }
    }

    let userKc = '';
    let arrayname = name.split(' ');
    let firstName = arrayname[0]; // get first name
    let lastName = arrayname[arrayname.length - 1] || '';

    try {
      userKc = await kcAdmin.users.create({
        realm: process.env.KC_REALM_NAME,
        username,
        email,
        firstName,
        lastName,
        // enabled required to be true in order to send actions email
        enabled: true,
        emailVerified: true,
      });

      await kcAdmin.users.resetPassword({
        id: userKc.id,
        realm: process.env.KC_REALM_NAME,
        credential: {
          temporary: false,
          type: 'password',
          value: password,
        },
      });
    } catch (err) {
      console.log('[ERROR]');
      console.log(err);

      return res.status(400).json({
        error: true,
      });
    }

    let userMongo;
    let kc_id = userKc.id;
    try {
      await User.createIndexes([
        {
          email,
          kc_id,
        },
      ]);
      userMongo = await User.create({
        kc_id,
        name,
        username,
        email,
        evaluation,
        interest,
        latitude,
        longitude,
        altitude,
        cep,
        online,
        phone,
        photo,
        type,
        conversations,
        notifications,
        origin,
      });
    } catch (err) {
      await kcAdmin.users.del({
        id: kc_id,
        realm: process.env.KC_REALM_NAME,
      });
      console.log(err);
      return res.status(400).json({
        ...err,
        error: true,
      });
    }

    return res.json({
      userMongo,
      userKc,
    });
  },
  async index(req, res) {
    const kc = await keycloakAdmin();

    let { email = false, id = false } = req.body;
    if (!email && !id) {
      return res.json({ error: 'Email or Id not provided' });
    }

    if (email) {
      try {
        const response = await User.findOne({
          email,
        });
        const [ressult] = await kc.users.find({
          email,
          realm: process.env.KC_REALM_NAME,
        });

        return res.json({
          mongodb: response,
          keycloak: ressult,
        });
      } catch (error) {}
      console.log(error);
      return res.json({
        error: 'not exists',
      });
    }
    if (id) {
      try {
        const response = await User.findOne({
          kc_id: id,
        });
        const result = await kc.users.findOne({
          id,
          realm: process.env.KC_REALM_NAME,
        });

        return res.json({
          mongodb: response,
          keycloak: result,
        });
      } catch (error) {
        console.log(error);
        return res.json({
          error: 'not exists',
        });
      }
    }
  },
  async update(req, res) {
    const {
      name,
      username,
      email,
      evaluation,
      interest,
      latitude,
      longitude,
      online,
      phone,
      photo,
      type,
      password,
      lastUpdateOrigin = 'undefined',
    } = req.body;
    try {
      const kc = await keycloakAdmin();

      let user = await kc.users.findOne({
        id: req.userId,
        realm: process.env.KC_REALM_NAME,
      });
      if (user) {
        let arrayname = name.split(' ');
        let first_name = arrayname[0]; // get first name
        let last_name = arrayname[arrayname.length - 1]; // get last name
        console.log(user);

        let usernameExists = await kc.users.find({
          username,
          realm: process.env.KC_REALM_NAME,
        });
        // tratando se username esta sendo usado por outro usuario
        if (usernameExists[0]) {
          if (usernameExists[0].id !== user.id) {
            return res.status(400).json({
              error: 'username already in use',
            });
          }
        }
        let emailExists = await kc.users.find({
          email,
          realm: process.env.KC_REALM_NAME,
        });
        // tratando se email esta sendo usado por outro usuario
        if (emailExists[0]) {
          if (emailExists[0].email !== user.email) {
            return res.status(400).json({
              error: 'email already in use',
            });
          }
        }
        await kc.users
          .update(
            {
              id: req.userId,
              realm: process.env.KC_REALM_NAME,
            },
            {
              firstName: first_name,
              lastName: last_name,
              username,
              attributes: {
                lastUpdateOrigin,
                ...user.attributes,
              },
              email,
              emailVerified: true,
              enabled: true,
            }
          )
          .then(async (e) => {
            if (password) {
              await kc.users.resetPassword({
                id: req.userId,
                realm: process.env.KC_REALM_NAME,
                credential: {
                  temporary: false,
                  type: 'password',
                  value: password,
                },
              });
            }

            let newUser = await kc.users.findOne({
              id: req.userId,
              realm: process.env.KC_REALM_NAME,
            });
            User.updateOne(
              { kc_id: req.userId },
              { ...req.body },
              (err, raw) => {
                if (err) {
                  return res.status(401).json({
                    error: 'fail to update user',
                  });
                }
                return res.status(200).json({ newUser, raw });
              }
            );
          });
      }
    } catch (error) {
      console.log(error);
      return res.send('ERRO');
    }
  },

  async remove(req, res) {
    try {
      let userId = req.params.id;
      let userData;
      const kcAdmin = await keycloakAdmin();
      userData = await kcAdmin.users.del({
        id: userId,
        realm: process.env.KC_REALM_NAME,
      });

      userData = await User.deleteOne({ kc_id: userId });

      return res.status(200).json({
        userData,
      });
    } catch (error) {
      console.log(error);
      return res.status(400).json(error.response.data);
    }
  },
  async disable(req, res) {
    let userId = req.params.id;
    const kcAdmin = await keycloakAdmin();
    if (req.userId !== userId) {
      return res.status(401).json({
        error: 'you are not allowed to disable that is not yours',
      });
    }
    await kcAdmin.users
      .update(
        {
          id: userId,
          realm: process.env.KC_REALM_NAME,
        },
        {
          enabled: false,
        }
      )
      .then(async () => {
        await User.updateOne(
          { kc_id: userId },
          {
            active: false,
          }
        );
      })
      .then((result) => {
        console.log(result);
        return res.status(200).json({ message: 'user disabled' });
      })
      .catch((err) => {
        console.log(err);
        return res.status(400).json({ error: 'fail to disable user' });
      });
  },
  async enable(req, res) {
    let userId = req.params.id;
    const kcAdmin = await keycloakAdmin();
    await kcAdmin.users
      .update(
        {
          id: userId,
          realm: process.env.KC_REALM_NAME,
        },
        {
          enabled: true,
        }
      )
      .then(async () => {
        await User.updateOne(
          { kc_id: userId },
          {
            active: true,
          }
        );
      })
      .then((result) => {
        console.log(result);
        return res.status(200).json({ message: 'user enabled' });
      })
      .catch((err) => {
        console.log(err);
        console.log(err);
        return res.status(400).json({ error: 'fail to enable user' });
      });
  },
};
