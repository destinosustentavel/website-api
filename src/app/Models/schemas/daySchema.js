import mongoose from 'mongoose';
mongoose.set('useCreateIndex', true);
const daySchema = new mongoose.Schema({
  day: {
    type: String,
    required: true,
  },
  initial_time: {
    type: String,
  },
  final_time: {
    type: String,
  },
});
export default daySchema;
