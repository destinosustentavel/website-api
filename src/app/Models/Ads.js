import mongoose from 'mongoose';
import daysSchema from './schemas/daySchema';
import coords from './schemas/coords';
mongoose.set('useCreateIndex', true);
const adsSchema = new mongoose.Schema(
  {
    // Pessoa Juridica
    PJ_associated: {
      type: String,
      default: null,
    },
    user_id: {
      type: String,
      required: true,
    }, // UsserID Keycloak
    user_name: {
      type: String,
      required: true,
    },
    description: {
      type: String,
    },
    location: [coords], // [lat, long, alti]
    type: {
      type: String,
      required: true,
    }, //Oferta, Pedido
    operation: {
      type: String,
      required: true,
    }, //Venda,Troca ou doacao
    days_of_the_week: [daysSchema],
    unity: {
      type: String,
      required: true,
    }, // Unidade de medida
    status: {
      type: String,
      required: true,
    }, //Disponivel,  Em andamento, Em transito, Concluido
    material_type: {
      type: String,
      required: true,
    }, //Papel,  Metal, plástico, vidro, eletronico, organico
    amount: {
      type: Number,
    },
    user_attending: {
      type: String,
      default: null,
    },
    material_subtype: {
      type: String,
    },
    photo: {
      type: String,
    },
    active: {
      type: Boolean,
      default: true
    },
  },
  {
    timestamps: true,
  } // created_at, updated_at
);

export default mongoose.model('Ads_Collection', adsSchema);
