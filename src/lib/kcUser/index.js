import { Issuer } from 'openid-client';
import keycloakClient from '../kcClient';

export default {
  async refresh(refresh_token) {
    const keycloakIssuer = await Issuer.discover(process.env.KC_BASE_URL_REALM);

    const client = new keycloakIssuer.Client({
      client_id: process.env.KC_CLIENT_ID, // Same as `clientId` passed to client.auth()
      client_secret: process.env.KC_CLIENT_SECRET,
    });

    try {
      // Use the grant type 'password'
      let tokenSet = await client.grant({
        grant_type: 'refresh_token',
        refresh_token,
      });

      // Periodically using refresh_token grant flow to get new access token here
      // setInterval(async () => {
      const refreshToken = tokenSet.refresh_token;
      tokenSet = await client.refresh(refreshToken);

      // }, 58 * 1000);
      const kcAdmin = await keycloakClient();

      kcAdmin.setAccessToken(tokenSet.access_token);
      return {
        acessToken: kcAdmin.accessToken,
        refreshToken: kcAdmin.refreshToken,
        error: false,
      };
    } catch (err) {
      console.log(err);
      return { ...err, error: true };
    }
  },
};
