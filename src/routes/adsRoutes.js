import {Router} from 'express';
import AdsController from '../app/Controllers/AdsController';

const adsRoutes = Router();
adsRoutes.post('/create', AdsController.create);
adsRoutes.get('/index', AdsController.index);
adsRoutes.get('/me', AdsController.indexByUser);
adsRoutes.patch('/update/:ad_id', AdsController.update);
adsRoutes.patch('/disable/:ad_id', AdsController.disable);
adsRoutes.patch('/enable/:ad_id', AdsController.enable);
adsRoutes.delete('/delete/:ad_id', AdsController.delete);

export default adsRoutes;