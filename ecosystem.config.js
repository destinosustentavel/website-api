module.exports = {
  apps : [
      {
        name: "api-backend",
        script: "./build/server.js",
        //instances: 2,
        exec_mode: "cluster",
        env: {
            "NODE_ENV": "development"
        },
	env_production: {
            "NODE_ENV": "production",
        }
      }
  ]
}
