import Ads from '../Models/Ads';
import User from '../Models/User';

export default {
  async create(req, res) {
    const { ad_id } = req.params;
    try {
      const response = await Ads.findById(ad_id);

      if (!response) {
        return res.status(400).json({
          error: 'ad not exists',
        });
      }
      const currentUser = await User.findOne({ kc_id: req.userId });
  
      // se existe usuario
      if (!currentUser) {
        return res.status(400).json({
          error: 'user not exists',
        });
      }
  
      // if ad.user_id == userId  return error : "you cannot subscribe to your own ad"
      if (response.user_id == currentUser._id) {
        return res
          .status(400)
          .json({ error: 'you cannot respond to an ad of your own' });
      }
  
      // if ad.user_attending == '' ok else error : 'ad is been atending by another user'
      if (
        response.user_attending != null ||
        response.user_attending.trim() != ''
      ) {
        return res.status(401).json({ error: 'ad already attended' });
      }
  
      const new_ad = await Ads.updateOne(
        {
          _id: ad_id,
        },
        { user_attending: currentUser._id }
      );
  
      return res.status(200).json(new_ad);

    } catch (error) {
      return res.status(400).json({
        error: 'invalid request',
      });
    }    
  },
};
