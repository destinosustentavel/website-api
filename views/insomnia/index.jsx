import React from 'react';

// import { Container } from './styles';

function insomnia() {
  return (
    <html>
    <head>
      <meta charSet="utf8"/>
      <meta name="generator" content="Insomnia Documenter v0.4.1"/>
      <meta name="viewport" content="width=device-width, user-scalable=no"/>
      <link rel="shortcut icon" href="favicon.ico"/>
      <link rel="stylesheet" href="bundle.css"/>
      <script defer src="bundle.js"></script>
      <link href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet"
        integrity="sha384-wvfXpqpZZVQGK6TAh5PVlGOfQNHSoD2xbE+QkPxCAFlNEevoEH3Sl0sibVcOQVnN" crossOrigin="anonymous"/>
    </head>
    <body>
      <noscript>In order to view this documentation page, you have to enable JavaScript in your web browser.</noscript>
      <div id="app"></div>
    </body>
    </html>
  );
}

export default insomnia;