import React from 'react';

function hello() {
  return (
    <h1 className="hello_h1">
      Welcome to DS Api!
    </h1>
  );
}

export default hello;
