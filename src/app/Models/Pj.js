import mongoose from 'mongoose';
mongoose.set('useCreateIndex', true);
const pjSchema = new mongoose.Schema(
  {
    fantasy: {
      type: String,
      required: true,
    },
    cnpj: {
      type: String,
      required: true,
    },
    email: {
      type: String,
      required: true,
    },
    phone: {
      type: String,
      required: true,
    },
    photo: {
      type: String,
    },
    type: {
      type: String,
      required: true,
    }, //'associacao / cooperativa / industria / empresa',
    user_responsible: {
      type: String,
      required: true,
    }, //'(id de quem criou a PJ)',
  },
  {
    timestamps: true,
  }
);

export default mongoose.model('Pj_Collection', pjSchema);
