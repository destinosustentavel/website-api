import express from 'express';
import cors from 'cors';
import path from 'path';
import { routes } from './routes';

import './bootstraps';
import './db';

const app = express();

app.use(express.json());
app.use(cors());

app.set('views', path.join(__dirname, '..', 'views'));
app.set('view engine', 'jsx');
app.engine('jsx', require('express-react-views').createEngine());

app.use(express.static(path.join(__dirname, '..', 'public')));
app.use(express.static(path.join(__dirname, '..', 'docs', 'insomnia')));


app.use(routes);

export { app };
