import Ads from '../Models/Ads';
import User from '../Models/User';
export default {
  async create(req, res) {
    const {
      PJ_associated,
      description,
      latitude,
      longitude,
      altitude,
      type,
      operation,
      days_of_the_week,
      unity,
      status,
      material_type,
      amount,
      user_attending,
      material_subtype,
      photo,
    } = req.body;

    // check required fields
    if (
      !(
      type && 
      operation && 
      days_of_the_week &&      
      status && 
      material_type 
      )
      ){
        return res.status(400).json({
          error: 'required fields are empty'
        });
    }
    const kc_id = req.userId;
    const userMongo = await User.findOne({ kc_id });

    const response = await Ads.create({
      PJ_associated,
      user_id: userMongo._id,
      user_name: userMongo.name,
      description,
      latitude,
      longitude,
      altitude,
      type,
      operation,
      days_of_the_week,
      unity,
      status,
      material_type,
      amount,
      user_attending,
      material_subtype,
      photo,
    }).catch((err) => {
      return res.status(500).json({
        error: 'failed to create ad',
        ...err.errors,
      });
    });

    return res.status(200).json(response);
  },
  async index(req, res) {
    const response = await Ads.find().where('active', true).catch((err) => {
      return res.status(500).json({
        error: 'failed to get ads',
        ...err.errors,
      });
    });

    return res.status(200).json(response);
  },
  async indexByUser(req, res) {
    const currentUser = await User.findOne().where({
      kc_id: req.userId
    });
    const response = await Ads.find().where({
        user_id :currentUser._id, 
        active: true
      }).catch((err) => {
      return res.status(500).json({
        error: 'failed to get ads',
        ...err.errors,
      });
    });

    return res.status(200).json(response);
  },
  async update(req, res) {
    const { ad_id } = req.params;
    if (!ad_id) {
      return res.status(401).json({
        error: 'ad id is requided',
      });
    }
    try {
      const currentAd = await Ads.findOne().where({
        _id: ad_id,
        active: true
      }).catch(() => {
        return res.status(400).json({
          error: 'invalid ad id',
        });
      });
      if(!currentAd){
        return res.status(200).json({
          message: "this ad not exists"
        })
      }

      const currentUser = await User.findOne({ kc_id: req.userId});
      if (!currentUser) {
        return res.status(400).json({ error: 'user not find' });
      }
      if (currentUser._id != currentAd.user_id) {
        return res.status(401).json({
          error: 'you are not the owner of this ad',
        });
      } else {
        const response = await Ads.updateOne(
          { _id: ad_id, user_id: currentUser._id },
          { ...req.body }
        );
        const newAd = await Ads.findById(ad_id);
        return res.status(200)
          .json({ ...newAd._doc, response: { ...response } });
      }
    } catch (error) {
      console.log(error);
      return res.status(500).json(error);
    }
  },
  async delete(req, res) {
    const { ad_id } = req.params;
    if (!ad_id) {
      return res.status(401).json({
        error: 'ad id is requided',
      });
    }

    const currentAd = await Ads.findById(ad_id).catch(() => {
      return res.status(400).json({
        error: 'invalid ad id',
      });
    });

    if(!currentAd){
      return res.status(400).json({
        message: "ad not found"
      })
    }

    const currentUser = await User.findOne({ kc_id: req.userId });
    if (!currentUser) {
      return res.status(400).json({ error: 'user not find' });
    }
    if (currentUser._id != currentAd.user_id) {
      return res.status(401).json({
        error: 'you are not the owner of this ad',
      });
    } else {
      const response = (
        await Ads.deleteOne({ _id: ad_id, user_id: currentUser._id })
      ).deletedCount;

      return res.status(200).json(response);
    }
  },
  async disable(req, res) {
    const { ad_id } = req.params;
    if (!ad_id) {
      return res.status(401).json({
        error: 'ad id is requided',
      });
    }

    const currentAd = await Ads.findById(ad_id).catch(() => {
      return res.status(400).json({
        error: 'invalid ad id',
      });
    });
    if(!currentAd){
      return res.status(400).json({
        message: "ad not found"
      })
    }

    const currentUser = await User.findOne({ kc_id: req.userId });
    if (!currentUser) {
      return res.status(400).json({ error: 'user not find' });
    }
    if (currentUser._id != currentAd.user_id) {
      return res.status(401).json({
        error: 'you are not the owner of this ad',
      });
    } else {
      let response = await Ads.updateOne({ 
        _id: ad_id, 
        user_id: currentUser._id, 
      }, 
      {
        active: false
      }
      ).catch(err => {
        return res.status(400).json({
          error: 'fail to disable ad'
        })
      });;

      return res.status(200).json({
        message: `ad is now disabled`
      });
    }
  },
  async enable(req, res) {
    const { ad_id } = req.params;
    if (!ad_id) {
      return res.status(401).json({
        error: 'ad id is requided',
      });
    }

    const currentAd = await Ads.findById(ad_id).catch(() => {
      return res.status(400).json({
        error: 'invalid ad id',
      });
    });
    if(!currentAd){
      return res.status(400).json({
        message: "ad not found"
      })
    }

    const currentUser = await User.findOne({ kc_id: req.userId });
    if (!currentUser) {
      return res.status(400).json({ error: 'user not find' });
    }
    if (currentUser._id != currentAd.user_id) {
      return res.status(401).json({
        error: 'you are not the owner of this ad',
      });
    } else {
      await Ads.updateOne({ 
        _id: ad_id, 
        user_id: currentUser._id, 
      }, 
      {
        active: true
      }
      ).catch(err => {
        return res.status(400).json({
          error: 'fail to enable ad'
        })
      });

      return res.status(200).json({
        message: `ad is now enabled`
      });
    }
  },
};
