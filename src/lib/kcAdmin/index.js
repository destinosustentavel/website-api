import KcAdminClient from 'keycloak-admin';
import '../../bootstraps';

export default async function keycloakAdmin(
  config = {
    baseUrl: process.env.KC_BASE_URL,
    realmName: 'master',
  }
) {
  const kcAdmin = new KcAdminClient(config);

  await kcAdmin
    .auth({
      username: process.env.KC_ADMIN_USER,
      password: process.env.KC_ADMIN_PASS,
      grantType: 'password',
      clientId: 'admin-cli',
      clientSecret: process.env.KC_ADMIN_CLI_SECRET,
    })
    .then(() => {
      console.log('[ADMIN LOGGED]');
    })
    .catch((err) => console.log(err));

  return kcAdmin;
}
