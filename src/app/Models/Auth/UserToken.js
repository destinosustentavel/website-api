import mongoose from 'mongoose';
mongoose.set('useCreateIndex', true);
const userTokenSchema = new mongoose.Schema(
  {
    kcId: {
      type: String,
      required: true,
      unique: true,
    },
    accessToken: {
      type: String,
      required: true,
    },
    refreshToken: {
      type: String,
      required: true,
    },
  },
  {
    timestamps: true,
  }
);

export default mongoose.model('userToken', userTokenSchema);
