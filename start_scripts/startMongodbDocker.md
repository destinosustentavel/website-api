# How to start the Docker container with your mongo image?

Run the following commad in your (Windows/Linux/Mac) terminal:

```
docker run -d -p 27017:27017 --name mongo_ds makusdouglas/mongo_ds
```

If you need a container from zero, do the following command:

```
docker run  \
 -e MONGO_INITDB_ROOT_USERNAME=myusername \
 -e MONGO_INITDB_ROOT_PASSWORD=mypassword \
 -e MONGO_INITDB_DATABASE=mydbname \
 -p 27017:27017 \
 mongo
```
