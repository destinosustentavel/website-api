import mongoose from 'mongoose';
mongoose.set('useCreateIndex', true);
const conversationSchema = new mongoose.Schema(
  {
    fromId: {
      type: String,
      required: true,
    },
    toId: {
      type: String,
      required: true,
    },
    text: {
      type: String,
      required: true,
    },
  },
  {
    timestamps: true,
  }
);
export default conversationSchema;
