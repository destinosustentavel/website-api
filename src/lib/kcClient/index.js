import kcClientAdmin from 'keycloak-admin';

async function keycloakClient() {
  const config = {
    baseUrl: process.env.KC_BASE_URL,
    realmName: process.env.KC_REALM_NAME,
  };
  const kcClient = new kcClientAdmin(config);

  await kcClient
    .auth({
      grantType: 'client_credentials',
      clientId: process.env.KC_CLIENT_ID,
      clientSecret: process.env.KC_CLIENT_SECRET,
    })
    .then(() => {
      console.log('[Client Logged]');
    })
    .catch((err) => {
      console.log(err.response);
    });

  return kcClient;
}
export default keycloakClient;
