import jwt from 'jsonwebtoken';
import fs from 'fs';
import path from 'path';

export default async (req, res, next) => {
  const authHeader = req.headers.authorization;
  if (!authHeader) {
    return res.status(401).json({ error: ' Token not provided' });
  }

  const [, token] = authHeader.split(' ');
  try {
    var pub = fs.readFileSync(
      path.join(__dirname, '..', '..', '..', 'certificates', 'pub.pem')
    );

    const decoded = jwt.verify(token, pub, {
      algorithms: ['RS256'],
      // ignoreExpiration: true
    });
    console.log(decoded);

    const {
      sub: subscription,
      session_state,
      exp,
      email,
      preferred_username,
    } = decoded;
    req.userId = subscription;
    req.userName = preferred_username;
    req.sessionState = session_state;
    req.expiration = exp;
    req.email = email;
  } catch (error) {
    return res.status(401).json({ error });
  }

  return next();
};
