import KeycloakAdminClient from 'keycloak-admin';

import Log from '../Models/Log';
import KeycloakAdmin from '../../lib/kcAdmin';
import KeycloakUser from '../../lib/kcUser';
import { decodeTokenJwt } from '../../lib/decodeTokenJwt';

export default {
  async create(req, res) {
    const { email, password } = req.body;
    const kcAdmin = await KeycloakAdmin();

    let [userData] = await kcAdmin.users
      .find({
        realm: process.env.KC_REALM_NAME,
        email: email,
      })
      .catch((err) => {
        console.log(err.response.data);
        return res.status(400).json({
          message: 'Internal server error, fail to connect to database',
          error: true,
        });
      });

    if (!userData) {
      return res.status(400).json({
        error: `Fail to find user ${email}`,
      });
    }
    if (!userData.enabled) {
      return res.status(401).json({
        error: 'User disabled, please contact us to enable you account',
      });
    }

    const config = {
      baseUrl: process.env.KC_BASE_URL,
      realmName: process.env.KC_REALM_NAME,
    };

    const kcAdminClient = new KeycloakAdminClient(config);

    try {
      await kcAdminClient.auth({
        username: email ? email : username,
        password,
        grantType: 'password',
        clientId: process.env.KC_CLIENT_ID,
        clientSecret: process.env.KC_CLIENT_SECRET,
      });
    } catch (err) {
      console.log(err);
      return res.status(400).json({
        error: true,
      });
    }

    const { accessToken, refreshToken } = kcAdminClient;

    const decodedToken = await decodeTokenJwt(accessToken);
    const exp = decodedToken ? decodedToken.exp : '';

    await Log.create({
      userId: userData.id,
      action: `user ${userData.email} signin`,
      from: 'SessionController.create',
    });
    return res.status(200).json({
      accessToken,
      refreshToken,
      exp,
      keyclockData: {
        ...userData,
      },
    });
  },

  async remove(req, res) {
    const kcAdmin = await KeycloakAdmin();

    var sessions;
    try {
      // kcAdmin.setAccessToken(token);
      sessions = await kcAdmin.users.listSessions({
        id: req.userId,
        realm: process.env.KC_REALM_NAME,
      });
      if (sessions[0]) {
        await kcAdmin.users.logout({
          id: req.userId,
          realm: process.env.KC_REALM_NAME,
        });

        await Log.create({
          userId: req.userId,
          action: `user ${req.email} logout`,
          from: 'SessionController.remove',
        });

        return res.status(200).json({
          message: 'logged out',
          sessions,
          error: false,
        });
      }
      return res.status(400).json({
        error: 'No more sessions for this user',
      });
    } catch (error) {
      console.log(error);
      return res.status(401).json({
        error: true,
      });
    }
  },
  async update(req, res) {
    const { refreshToken } = req.body;

    if (!refreshToken) {
      return res.status(400).json({
        message: 'Refresh token not provided',
        error: true,
      });
    }

    const response = await KeycloakUser.refresh(refreshToken);

    return res.json(response);
  },
};
