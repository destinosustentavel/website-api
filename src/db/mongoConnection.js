import mongoose from 'mongoose';

function mongoConnection() {
  mongoose.connect(process.env.MONGO_URL, {
    useNewUrlParser: true,
    useFindAndModify: false,
    useUnifiedTopology: true,
    monitorCommands: true,
  });
  mongoose.connection
    .on('error', console.error.bind(console, 'connection error:'))
    .once('open', function () {
      console.log(
        `mongoDb online in ${process.env.NODE_ENV} MODE at DATABASE: ${mongoose.connection.db.databaseName}`
      ); // we're connected!
    });
}
export { mongoConnection };
