import mongoose from 'mongoose';
mongoose.set('useCreateIndex', true);
const notificationSchema = new mongoose.Schema(
  {
    viewed: {
      type: Boolean,
      default: false,
    },
    action: {
      type: String,
      required: true,
    },
    adId: {
      type: String,
      required: true,
    },
    from: {
      type: String,
      required: true,
    },
  },
  {
    timestamps: true,
  }
);
export default notificationSchema;
